# File cap.mod
param n;
param probability{i in 1..n};
param demand_u{i in 1..n};
param demand_v{i in 1..n};

var x_r_u integer >= 0, <= 12;
var x_r_v integer >= 0, <= 12;
var x_r_t integer >= 0, <= 12;
var y_r_u{1..n} >= 0;
var y_r_v{1..n} >= 0;
var y_r_t{1..n} >= 0;
var y_t_u{1..n} >= 0;
var y_t_v{1..n} >= 0;
var z_u{1..n} >= 0;
var z_v{1..n} >= 0;

minimize ObjPlusRecourse: 4 * x_r_u + 3 * x_r_v + 5 * x_r_t + 
		sum{s in 1..n} probability[s] * 8 * (z_u[s] + z_v[s]);

subject to Capacity_Constraint_r_u{s in 1..n}:
	y_r_u[s] <= 5 + x_r_u;

subject to Capacity_Constraint_r_v{s in 1..n}:
	y_r_v[s] <= 7 + x_r_v;

subject to Capacity_Constraint_r_t{s in 1..n}:
	y_r_t[s] <= 11 + x_r_t;

subject to Capacity_Constraint_t_u{s in 1..n}:
	y_t_u[s] <= 12;

subject to Capacity_Constraint_t_v{s in 1..n}:
	y_t_v[s] <= 16;

subject to Flow_Conservation_t{s in 1..n}:
	y_r_t[s] = y_t_u[s] + y_t_v[s];

subject to Unmet_Demand_u{s in 1..n}:
	z_u[s] = demand_u[s] - y_r_u[s] - y_t_u[s];

subject to Unmet_Demand_v{s in 1..n}:
	z_v[s] = demand_v[s] - y_r_v[s] - y_t_v[s];



