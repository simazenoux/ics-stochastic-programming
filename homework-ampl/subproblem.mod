param xru := 0;
param xrv := 0;
param xrt := 0;
param dsu := 15;
param dsv := 8;

# Define the decision variables
var lambda{1..11} >= 0;

maximize obj : (5+xru) * lambda[1] + (7+ xrv) * lambda[2] + (11 + xrt) * lambda[3] + 12 * lambda[4] + 16 * lambda[5] - dsu * lambda[8] - dsu * lambda[9] - dsv * lambda[10] - dsv * lambda[11];
subject to c1:
    -lambda[1] - lambda[8] + lambda[9] >= 0;
subject to c2:
    -lambda[2] + lambda[10] - lambda[11] >= 0;
subject to c3:
    -lambda[3] - lambda[6] + lambda[7] >= 0;
subject to c4:
    -lambda[4] + lambda[6] - lambda[7] - lambda[8] + lambda[9] >= 0;
subject to c5:
    -lambda[5] + lambda[6] - lambda[7] + lambda[10] - lambda[11] >= 0;
subject to c6:
    -lambda[8] + lambda[9] <= 8;
subject to c7:
    lambda[10] - lambda[11] <= 8;



minimize 