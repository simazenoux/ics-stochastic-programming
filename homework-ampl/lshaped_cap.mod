param n;
param probability{i in 1..n};
param demand_u{i in 1..n};
param demand_v{i in 1..n};

param nCut >= 0 integer;
param scenIx integer;


var x_r_u integer >= 0, <= 12;
var x_r_v integer >= 0, <= 12;
var x_r_t integer >= 0, <= 12;
var y_r_u{1..n} >= 0;
var y_r_v{1..n} >= 0;
var y_r_t{1..n} >= 0;
var y_t_u{1..n} >= 0;
var y_t_v{1..n} >= 0;
var z_u{1..n} >= 0;
var z_v{1..n} >= 0;
var theta >= 0;


param tcoef0{1..n} default 0;
param tcoef1{1..n} default 0;
param tcoef2{1..n} default 0;
param tcoef3{1..n} default 0;

param coef0{1..nCut} default 0;
param coef1{1..nCut} default 0;
param coef2{1..nCut} default 0;
param coef3{1..nCut} default 0;


minimize SubObj{s in 1..n}:
    8 * z_u[s] + 8 * z_v[s];

subject to Capacity_Constraint_r_u{s in 1..n}:
	y_r_u[s] <= 5 + x_r_u;

subject to Capacity_Constraint_r_v{s in 1..n}:
	y_r_v[s] <= 7 + x_r_v;

subject to Capacity_Constraint_r_t{s in 1..n}:
	y_r_t[s] <= 11 + x_r_t;

subject to Capacity_Constraint_t_u{s in 1..n}:
	y_t_u[s] <= 12;

subject to Capacity_Constraint_t_v{s in 1..n}:
	y_t_v[s] <= 16;

subject to Flow_Conservation_t{s in 1..n}:
	y_r_t[s] = y_t_u[s] + y_t_v[s];

subject to Unmet_Demand_u{s in 1..n}:
	z_u[s] = demand_u[s] - y_r_u[s] - y_t_u[s];

subject to Unmet_Demand_v{s in 1..n}:
	z_v[s] = demand_v[s] - y_r_v[s] - y_t_v[s];



minimize MasterObj:
     4 * x_r_u + 3 * x_r_v + 5 * x_r_t + theta;

subject to Cuts {k in 1..nCut}:
	theta >= coef0[k] - coef1[k] * x_r_u - coef2[k] * x_r_v - coef3[k] * x_r_t;
