param nSCEN;
set SCEN;

param nCut >= 0 integer;
param scenIx integer;

var x1 >= 0, := 2;
var x2 >= 0, := 2;
var theta >= 0;  #(Since q >=0, y >= 0)

var y1{SCEN} >= 0;
var y2{SCEN} >= 0;


param q1;
param q2;
param p{SCEN} default 1/card(SCEN);
param omega1{SCEN};
param omega2{SCEN};



minimize deterministic_obj:
    x1 + x2 + sum{i in SCEN} p[i] * (q1 * y1[i] + q2 * y2[i];

subject to dc1{i in SCEN}:
    omega1[i] * x1 + x2 + y1[i] >= 7;

subject to dc2{i in SCEN}:
    omega2[i] * x1 + x2 + y2[i] >= 4;